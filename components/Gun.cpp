#include "components\Gun.h"

Gun::Gun()
{
}

Gun::Gun(double _weaponCooldown, double _directionX, double _directionY, double _originX, double _originY, bool _firing) 
	:weaponCooldown(_weaponCooldown), 
	directionX(_directionX), 
	directionY(_directionY),
	originX(_originX),
	originY(_originY),
	firing(_firing)
{
}
