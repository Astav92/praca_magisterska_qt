#pragma once
#include "entityx\Entity.h"

struct Background : public entityx::Component<Background>
{
	Background();

	Background(double _ySpeed, int _startY);

	double ySpeed;

	int startingPositionY;
};
