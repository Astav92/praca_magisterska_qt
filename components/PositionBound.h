#pragma once
#include "entityx\Entity.h"

struct PositionBound : public entityx::Component<PositionBound>
{
	PositionBound();
	PositionBound(int _xLow, int _yLow, int _xHigh, int _yHigh);
	int x_low;
	int y_low;
	int x_high;
	int y_high;
};
