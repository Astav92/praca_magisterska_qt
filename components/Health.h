#pragma once
#include "entityx\Entity.h"

struct Health : public entityx::Component<Health>
{
	Health();

	Health(int _health);

	int health;
};