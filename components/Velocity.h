#pragma once
#include "entityx\Entity.h"

struct Velocity : public entityx::Component<Velocity>
{
	Velocity();

	Velocity(double _x, double _y);

	double x;
	double y;
};