#pragma once
#include "entityx\Entity.h"

struct Bullet : public entityx::Component<Bullet>
{
	Bullet();

	Bullet(int _damage);

	int damage;
};
