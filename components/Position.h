#pragma once
#include "entityx\Entity.h"

struct Position : public entityx::Component<Position>
{
	Position();

	Position(double _x, double _y);

	double x;

	double y;
};