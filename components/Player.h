#pragma once
#include "entityx\Entity.h"

struct Player : public entityx::Component<Player>
{
	Player();

	Player(int id);

public:
	int playerId;
};
