#pragma once
#include "entityx\Entity.h"

struct Gun : public entityx::Component<Gun>
{
	Gun();

	Gun(double _weaponCooldown, double _directionX, double _directionY,double _originX, double _originY, bool _firing);

	double shootTimer = 0;
	double weaponCooldown;

	double directionX;
	double directionY;

	double originX;
	double originY;

	bool firing;
};
