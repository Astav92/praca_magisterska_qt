#pragma once
#include "entityx\Entity.h"
#include "Rect2D.h"

struct BoxCollider : public entityx::Component<BoxCollider>
{
	BoxCollider();

	BoxCollider(double _sizeX, double _sizeY, double _offsetX, double _offsetY);

	double sizeX;
	double sizeY;

	double offsetX;
	double offsetY;
};