#pragma once
#include "entityx\Entity.h"

struct Asset : public entityx::Component<Asset>
{
	Asset();

	Asset(int _x, int _y, std::string _path);

	int sizeX, sizeY;

	std::string resourcePath;
};
