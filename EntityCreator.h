#pragma once
#include "entityx\Entity.h"

class BulletCreator
{
public:
	static void create(entityx::Entity entity);
};

class EnemyCreator
{
public:
	static void create(entityx::Entity entity);
};

class AsteroidCreator
{
public:
	static void create(entityx::Entity entity);
};

class PlayerCreator
{
public:
	static void create(entityx::Entity entity, int id);
};

