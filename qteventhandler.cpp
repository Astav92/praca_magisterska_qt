#include <qteventhandler.h>
//#include "GameManager.h"
#include "Input.h"

/*
 * This class handles interactions with the text field
 */
QtEventHandler::QtEventHandler(QObject *parent) :
    QObject(parent)
{
}

void QtEventHandler::playerOneClicked()
{
    GameManager::Instance->StartGame(1);
}

void QtEventHandler::playerTwoClicked()
{
  GameManager::Instance->StartGame(2);
}

// obsługa wciśnięcia klawisza
void QtEventHandler::keyPressed(int key)
{
  switch (key)
    {
    case Qt::Key_W:
    Input::KEY_UP_1 = true;
    break;
    case Qt::Key_A:
    Input::KEY_LEFT_1 = true;
    break;
    case Qt::Key_S:
    Input::KEY_DOWN_1 = true;
    break;
    case Qt::Key_D:
    Input::KEY_RIGHT_1 = true;
    break;

    case Qt::Key_Up:
    Input::KEY_UP_2 = true;
    break;
    case Qt::Key_Down:
    Input::KEY_DOWN_2 = true;
    break;
    case Qt::Key_Left:
    Input::KEY_LEFT_2 = true;
    break;
    case Qt::Key_Right:
    Input::KEY_RIGHT_2 = true;
    break;

    case Qt::Key_Control:
    Input::KEY_FIRE_2 = true;
    break;
    case Qt::Key_Space:
    Input::KEY_FIRE_1 = true;
    break;
    }
}

// obsługa puszczenia klawisza
void QtEventHandler::keyReleased(int key)
{
  switch (key)
    {
    case Qt::Key_W:
    Input::KEY_UP_1 = false;
    break;
    case Qt::Key_A:
    Input::KEY_LEFT_1 = false;
    break;
    case Qt::Key_S:
    Input::KEY_DOWN_1 = false;
    break;
    case Qt::Key_D:
    Input::KEY_RIGHT_1 = false;
    break;

    case Qt::Key_Up:
    Input::KEY_UP_2 = false;
    break;
    case Qt::Key_Down:
    Input::KEY_DOWN_2 = false;
    break;
    case Qt::Key_Left:
    Input::KEY_LEFT_2 = false;
    break;
    case Qt::Key_Right:
    Input::KEY_RIGHT_2 = false;
    break;

    case Qt::Key_Control:
    Input::KEY_FIRE_2 = false;
    break;
    case Qt::Key_Space:
    Input::KEY_FIRE_1 = false;
    break;
    }
}
