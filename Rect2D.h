#pragma once

struct Rect
{
	Rect(double _left, double _right, double _top, double _bottom);


	double left;
	double right;
	double top;
	double bottom;
};
