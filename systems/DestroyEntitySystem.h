#pragma once
#include "entityx/System.h"

class DestroyEntitySystem : public entityx::System<DestroyEntitySystem>
{
public:
	DestroyEntitySystem();

	void update(entityx::EntityManager & es, entityx::EventManager & events, double dt);
};

