#include "BackgroundSystem.h"
#include "components\Position.h"
#include "components\Background.h"
#include "components\Asset.h"

using namespace entityx;
using namespace std;

BackgroundSystem::BackgroundSystem()
{
}

void BackgroundSystem::update(entityx::EntityManager & entities, entityx::EventManager & events, double dt)
{
	Position::Handle position;
	Background::Handle background;
	Asset::Handle asset;

	for (Entity entity : entities.entities_with_components(position, background, asset))
	{
		double newY(position->y + background->ySpeed * dt);

		position->y = newY;

        if (abs((background->startingPositionY - position->y)) > asset->sizeY)
		{
			position->y = background->startingPositionY;
		}
	}
}


