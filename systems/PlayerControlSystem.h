#pragma once
#include "entityx\System.h"

class PlayerControlSystem : public entityx::System<PlayerControlSystem>
{
public:
	PlayerControlSystem();

	void update(entityx::EntityManager &entities, entityx::EventManager &events, double dt);
};

