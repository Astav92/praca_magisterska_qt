#pragma once
#include "entityx\System.h"

class GunSystem : public entityx::System<GunSystem>
{
public:
	GunSystem();

	void update(entityx::EntityManager & es, entityx::EventManager & events, double dt);
};

