#include "DestroyOutOfScreenEntities.h"
#include "components\Bullet.h"
#include "components\Position.h"
#include "components\Destroy.h"
#include "components\Asset.h"
#include "components\Velocity.h"

#include "ScreenSize.h"

using namespace entityx;

int Bound = 600;

DestroyOutOfScreenEntities::DestroyOutOfScreenEntities()
{
}

void DestroyOutOfScreenEntities::update(entityx::EntityManager & es, entityx::EventManager & events, double dt)
{
	Position::Handle position;
	Asset::Handle asset;
	Velocity::Handle velocity;

	for (Entity entity : es.entities_with_components(position, asset, velocity))
	{
		if (position->x > ScreenSize::width+Bound
			|| position->x < -asset->sizeX-Bound
			|| position->y > ScreenSize::height+Bound
			|| position->y < -asset->sizeY-Bound)
		{
			entity.assign<Destroy>();
		}
	}
}