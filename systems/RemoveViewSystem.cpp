#include "RemoveViewSystem.h"
#include "components\View.h"
#include "Game.h"


RemoveViewSystem::RemoveViewSystem()
{
}

void RemoveViewSystem::configure(entityx::EventManager & event_manager)
{
	event_manager.subscribe<entityx::EntityDestroyedEvent>(*this);
}

void RemoveViewSystem::update(entityx::EntityManager & entities, entityx::EventManager & events, double dt)
{
	// dummy system
}

void RemoveViewSystem::receive(const entityx::EntityDestroyedEvent & event)
{
	if (event.entity.has_component<View>())
	{
		entityx::Entity en = event.entity;

		View::Handle viewComponent = en.component<View>();

		delete viewComponent->view;

		Game::view_count--;
	}
}

