#pragma once
#include "entityx\System.h"

class PositionBoundSystem : public entityx::System<PositionBoundSystem>
{
public:
	PositionBoundSystem();

	void update(entityx::EntityManager & es, entityx::EventManager & events, double dt);
};

