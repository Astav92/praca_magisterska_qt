#pragma once
#include "entityx\System.h"
#include "entityx\Event.h"
#include "Events.h"
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickItem>

class RenderSystem : public entityx::System<RenderSystem>, public entityx::Receiver<RenderSystem>
{
public:
    RenderSystem(QGuiApplication& app, QQmlApplicationEngine& engine, entityx::EventManager &ev_manager);

	void configure(entityx::EventManager &event_manager);

	void update(entityx::EntityManager &entities, entityx::EventManager &events, double dt);

	void receive(const EvGameEnded &ev);

	void receive(const EvGameStarted &ev);

	void receive(const EvUiChanged &ev);

	void HideGameHud();

	void ShowGameHud();

	void HideMenu();

	void ShowMenu();

    void RefreshGameHud();
    static RenderSystem *Instance;
    static QGuiApplication  *m_app;
    static QQmlApplicationEngine *m_engine;
    static QQuickWindow *m_window;
    static QObject *m_game_container;
    static QQuickItem *m_item_container;

private:
    entityx::EventManager &m_eventManager;
};

