#pragma once
#include "entityx\System.h"
#include "components\Asset.h"

class AddViewSystem : public entityx::System<AddViewSystem>, public entityx::Receiver<AddViewSystem>
{
public:
	
	AddViewSystem();

	void configure(entityx::EventManager &event_manager);

	void update(entityx::EntityManager &entities, entityx::EventManager &events, double dt);

	void receive(const entityx::ComponentAddedEvent<Asset> &event);
};

