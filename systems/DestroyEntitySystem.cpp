#include "DestroyEntitySystem.h"
#include "components\Destroy.h"

using namespace entityx;

DestroyEntitySystem::DestroyEntitySystem()
{
}

void DestroyEntitySystem::update(entityx::EntityManager & es, entityx::EventManager & events, double dt)
{
	Destroy::Handle destroy;

	for (Entity entity : es.entities_with_components(destroy))
	{
		entity.destroy();
	}
}
