#include "SpawnEnemiesSystem.h"
#include <cstdlib>
#include <ctime>
#include "components\Position.h"
#include "components\Velocity.h"
#include "EntityCreator.h"
#include "ScreenSize.h"
#include "GameManager.h"

bool spawnAsteroid = false;

SpawnEnemiesSystem::SpawnEnemiesSystem()
{
	srand(time(NULL));
	m_scrollSpeed = 70;
	//(std::rand() % ile_liczb_w_przedziale ) + startowa_liczba;
}

void SpawnEnemiesSystem::update(entityx::EntityManager & es, entityx::EventManager & events, double dt)
{
	m_levelOffset += dt * m_scrollSpeed;

	if (GameManager::State != GameState::Gameplay)
	{
		return;
	}

	if (m_levelOffset > 80000000)
	{
		m_levelOffset = 0;

		// spawn enemy z randomowa lokacja

		spawnAsteroid = !spawnAsteroid;

		auto entity = es.create();
		auto entity2 = es.create();

		if (spawnAsteroid)
		{
			AsteroidCreator::create(entity);
			AsteroidCreator::create(entity2);
		}
		else
		{
			EnemyCreator::create(entity);
			EnemyCreator::create(entity2);
		}
		

		double positionY = -100;
		double positionX = (std::rand() % ScreenSize::width - 100) + 100;
		entity.replace<Position>(positionX, positionY);
		positionX = (std::rand() % ScreenSize::width - 100) + 100;
		entity2.replace<Position>(positionX, positionY);

		double velocityY = (std::rand() % 150) + 100;
		entity.replace<Velocity>(0, velocityY);
		velocityY = (std::rand() % 150) + 100;
		entity2.replace<Velocity>(0, velocityY);
	}
}
