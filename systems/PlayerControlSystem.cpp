#include "PlayerControlSystem.h"
#include "components\Player.h"
#include "components\Velocity.h"
#include "components\Gun.h"
#include "Input.h"

using namespace entityx;

PlayerControlSystem::PlayerControlSystem()
{
}

void PlayerControlSystem::update(entityx::EntityManager &entities, entityx::EventManager &events, double dt)
{
	float velocity = 300.0f;

	Player::Handle playerComponent;
	Velocity::Handle velocityComponent;
	Gun::Handle gunComponent;

	for (Entity entity : entities.entities_with_components(velocityComponent, playerComponent, gunComponent))
	{
		if (playerComponent->playerId == 1)
		{
			velocityComponent->x = Input::KEY_LEFT_1 && Input::KEY_RIGHT_1 ? 0 : Input::KEY_LEFT_1 ? -velocity : Input::KEY_RIGHT_1 ? velocity : 0;

			velocityComponent->y = Input::KEY_UP_1 && Input::KEY_DOWN_1 ? 0 : Input::KEY_UP_1 ? -velocity : Input::KEY_DOWN_1 ? velocity : 0;

			gunComponent->firing = Input::KEY_FIRE_1;
		}
		else
		{
			velocityComponent->x = Input::KEY_LEFT_2 && Input::KEY_RIGHT_2 ? 0 : Input::KEY_LEFT_2 ? -velocity : Input::KEY_RIGHT_2 ? velocity : 0;

			velocityComponent->y = Input::KEY_UP_2 && Input::KEY_DOWN_2 ? 0 : Input::KEY_UP_2 ? -velocity : Input::KEY_DOWN_2 ? velocity : 0;

			gunComponent->firing = Input::KEY_FIRE_2;
		}
	}
}


