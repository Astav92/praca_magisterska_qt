#pragma once
#include "entityx\System.h"

class BackgroundSystem : public entityx::System<BackgroundSystem>
{
public:
	BackgroundSystem();

	void update(entityx::EntityManager & entities, entityx::EventManager & events, double dt);
};

