#include "PositionBoundSystem.h"
#include "components\Position.h"
#include "components\PositionBound.h"

using namespace entityx;

PositionBoundSystem::PositionBoundSystem()
{
}

void PositionBoundSystem::update(entityx::EntityManager & es, entityx::EventManager & events, double dt)
{
	Position::Handle position;
	PositionBound::Handle bound;

	for (Entity entity : es.entities_with_components(position, bound))
	{
		position->x = position->x > bound->x_high ? bound->x_high : position->x < bound->x_low ? bound->x_low : position->x;
		position->y = position->y > bound->y_high ? bound->y_high : position->y < bound->y_low ? bound->y_low : position->y;
	}
}

