#include "systems/RenderSystem.h"
#include "components\View.h"
#include "components\Position.h"
#include "Input.h"
#include "ScreenSize.h"
#include <string>
#include "GameManager.h"
#include "qteventhandler.h"
#include "Events.h"
#include "entityx/Event.h"

#include <QGuiApplication>
#include <QQmlApplicationEngine>

using namespace entityx;

std::string pl_1_lives_string = "Player 1 lives: ";
std::string pl_2_lives_string = "Player 2 lives: ";
std::string score_string = "Score: ";

RenderSystem *RenderSystem::Instance = nullptr;
QGuiApplication *RenderSystem::m_app = nullptr;
QQmlApplicationEngine *RenderSystem::m_engine = nullptr;
QQuickWindow *RenderSystem::m_window = nullptr;
QObject *RenderSystem::m_game_container = nullptr;
QQuickItem *RenderSystem::m_item_container = nullptr;
QtEventHandler eventHandler;

int frameCount = 0;

RenderSystem::RenderSystem(QGuiApplication &app, QQmlApplicationEngine &engine, entityx::EventManager &ev_manager):m_eventManager(ev_manager)
{
    RenderSystem::m_app = &app;
    RenderSystem::m_engine = &engine;
    QObject *rootObject = engine.rootObjects().first();
    m_window = qobject_cast<QQuickWindow*>(rootObject);
    m_item_container = qobject_cast<QQuickItem*>(m_window->children().at(1));
    m_game_container = m_window->children().at(1);
    Instance = this;

    QObject::connect(m_window, SIGNAL(playerOneClicked()), &eventHandler, SLOT(playerOneClicked()));
    QObject::connect(m_window, SIGNAL(playerTwoClicked()), &eventHandler, SLOT(playerTwoClicked()));
    // RenderSystem.cpp
    // zapięcie się na sygnały klawiatury
    QObject::connect(m_window, SIGNAL(keyPressed(int)), &eventHandler, SLOT(keyPressed(int)));
    QObject::connect(m_window, SIGNAL(keyReleased(int)), &eventHandler, SLOT(keyReleased(int)));

    HideGameHud();
}


void RenderSystem::configure(entityx::EventManager & event_manager)
{
	event_manager.subscribe<EvGameStarted>(*this);
	event_manager.subscribe<EvGameEnded>(*this);
	event_manager.subscribe<EvUiChanged>(*this);
}

void RenderSystem::update(entityx::EntityManager & entities, entityx::EventManager & events, double dt)
{
	View::Handle viewComponent;
	Position::Handle positionComponent;

	for (Entity entity : entities.entities_with_components(viewComponent, positionComponent))
	{
        viewComponent->view->setX(positionComponent->x);
        viewComponent->view->setY(positionComponent->y);
	}

	frameCount++;

	if (frameCount == 30)
	{
        int fps = 1 / dt;
		frameCount = 0;
    }

    m_app->processEvents();
}

void RenderSystem::receive(const EvGameEnded & ev)
{
        HideGameHud();
}

void RenderSystem::receive(const EvGameStarted & ev)
{
	ShowGameHud();
}

void RenderSystem::receive(const EvUiChanged & ev)
{
	RefreshGameHud();
}

void RenderSystem::ShowMenu()
{
//nic
}

void RenderSystem::HideMenu()
{
  // nic
}

void RenderSystem::ShowGameHud()
{
QMetaObject::invokeMethod(m_game_container, "showHud");
}

void RenderSystem::HideGameHud()
{
QMetaObject::invokeMethod(m_game_container, "hideHud");
}

void RenderSystem::RefreshGameHud()
{
QMetaObject::invokeMethod(m_game_container, "setPlayerOneHp", Q_ARG(QVariant, GameManager::Player1Lives));
QMetaObject::invokeMethod(m_game_container, "setPlayerTwoHp", Q_ARG(QVariant, GameManager::Player2Lives));
QMetaObject::invokeMethod(m_game_container, "setScore", Q_ARG(QVariant, GameManager::Score));
}
