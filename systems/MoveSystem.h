#pragma once

#include "entityx\System.h"


class MoveSystem : public entityx::System<MoveSystem>
{
public:
	MoveSystem();

	void update(entityx::EntityManager &entities, entityx::EventManager &events, double dt);
};

