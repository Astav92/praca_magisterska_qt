#include "CollisionSystem.h"
#include "components\Position.h"
#include "components\BoxCollider.h"
#include "components\Bullet.h"
#include "components\Player.h"
#include "components\Enemy.h"
#include "components\Health.h"
#include "components\Destroy.h"
#include "Events.h"
#include "Rect2D.h"


using namespace entityx;

CollisionSystem::CollisionSystem()
{
}

void CollisionSystem::update(entityx::EntityManager & es, entityx::EventManager & events, double dt)
{
	Position::Handle posL;
	BoxCollider::Handle colL;

	Position::Handle posR;
	BoxCollider::Handle colR;

	Bullet::Handle bullet;
	Player::Handle player;
	Enemy::Handle enemy;

	// pociski gracza vs przeciwnicy
	for (Entity playerBulletEntity : es.entities_with_components(posL, colL, bullet, player))
	{
		for (Entity enemyEntity : es.entities_with_components(posR, colR, enemy))
		{
			if (DetectCollision(CreateRect(colL,posL), CreateRect(colR,posR)))
			{
				Health::Handle health = enemyEntity.component<Health>();
				if (health)
				{
					health->health -= bullet->damage;

					if (health->health <= 0)
					{
						enemyEntity.assign<Destroy>();
						events.emit<EvPlayerKilledEnemy>();
					}

					playerBulletEntity.assign < Destroy>();
				}
			}
		}
	}

	for (Entity enemy : es.entities_with_components(posL, colL, enemy))
	{
		for (Entity pl : es.entities_with_components(posR, colR, player))
		{
			if (DetectCollision(CreateRect(colL, posL), CreateRect(colR, posR)))
			{
				Bullet::Handle bull = enemy.component<Bullet>();
				Bullet::Handle playerBull = pl.component<Bullet>();
				
				if (playerBull)
				{
					// przeciwnik zderzyl sie z pociskiem gracza. to juz zostalo obsluzone
					continue;
				}

				enemy.assign<Destroy>();

				if (bull)
				{
					
					// pocisk przeciwnika uderzyl gracza
					events.emit<EvPlayerReceivedDamage>(bull->damage, player->playerId);
				}
				else
				{
					// przeciwnik uderzyl gracza
					events.emit<EvPlayerReceivedDamage>(10, player->playerId);
				}
			}
		}
	}
}

bool CollisionSystem::DetectCollision(Rect a, Rect b)
{
	// TOP-LEFT ORIGIN
	return !(b.left > a.right || b.right < a.left || b.top > a.bottom || b.bottom < a.top);
}

Rect CollisionSystem::CreateRect(BoxCollider::Handle col, Position::Handle pos)
{
	return Rect(pos->x + col->offsetX,
		pos->x + col->offsetX + col->sizeX,
		pos->y + col->offsetY,
		pos->y + col->offsetY + col->sizeY);
}
