#include "MoveSystem.h"
#include "components\Position.h"
#include "components\Velocity.h"

using namespace entityx;

MoveSystem::MoveSystem()
{
}

void MoveSystem::update(entityx::EntityManager & entities, entityx::EventManager & events, double dt)
{
	Position::Handle position;
	Velocity::Handle velocity;

	for (Entity entity : entities.entities_with_components(position,velocity))
	{
		double newX(position->x + velocity->x * dt);
		double newY(position->y + velocity->y * dt);

		position->x = newX;
		position->y = newY;
	}
}


