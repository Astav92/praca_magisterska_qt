#pragma once
#include "entityx\System.h"

class SpawnEnemiesSystem : public entityx::System<SpawnEnemiesSystem>
{
public:
	SpawnEnemiesSystem();

	void update(entityx::EntityManager & es, entityx::EventManager & events, double dt);

private:

	double m_scrollSpeed;
	double m_levelOffset;
};

