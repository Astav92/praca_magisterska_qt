#include "GunSystem.h"
#include "Input.h"
#include "components\Position.h"
#include "components\Gun.h"
#include "components\Enemy.h"
#include "components\Player.h"
#include "components\Velocity.h"
#include "EntityCreator.h"

using namespace entityx;

GunSystem::GunSystem()
{
}

void GunSystem::update(entityx::EntityManager & es, entityx::EventManager & events, double dt)
{
	Position::Handle position;
	Gun::Handle gun;

	for (Entity entity : es.entities_with_components(position, gun))
	{
		gun->shootTimer -= dt;

		if (gun->firing && gun->shootTimer > 0)
		{
			continue;
		}

		if (gun->firing == false)
		{
			continue;
		}

		gun->shootTimer = gun->weaponCooldown;

		auto bullet = es.create();
		BulletCreator::create(bullet);
		bullet.replace<Position>(position->x+gun->originX, position->y+gun->originY);

		if (entity.has_component<Player>())
		{
			bullet.assign<Player>();
			bullet.replace<Velocity>(0, -500);
		}
		else {
			bullet.assign<Enemy>();
			bullet.replace<Velocity>(0, 500);
		}
	}
}

