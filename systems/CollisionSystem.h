#pragma once
#include "entityx/System.h"
#include "Rect2D.h"
#include "components\Position.h"
#include "components\BoxCollider.h"

class CollisionSystem : public entityx::System<CollisionSystem>
{
public:
	CollisionSystem();

	void update(entityx::EntityManager &es, entityx::EventManager &events, double dt);

	bool DetectCollision(Rect r1, Rect r2);

	Rect CreateRect(BoxCollider::Handle col, Position::Handle pos);

};

