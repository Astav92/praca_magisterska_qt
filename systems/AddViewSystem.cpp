#include "systems/AddViewSystem.h"
#include "components\View.h"
#include "components\Asset.h"
#include "systems/RenderSystem.h"
#include "entityx\Entity.h"
#include <QtQuick>
#include "Game.h"

using namespace entityx;

AddViewSystem::AddViewSystem()
{
}

void AddViewSystem::configure(entityx::EventManager & event_manager)
{
	// subskrybcja na sygnał o dodaniu komponentu
	event_manager.subscribe<entityx::ComponentAddedEvent<Asset>>(*this);
}

void AddViewSystem::update(entityx::EntityManager & entities, entityx::EventManager & events, double dt)
{
}

// odebranie sygnału
void AddViewSystem::receive(const entityx::ComponentAddedEvent<Asset>& event)
{
    Entity entity = event.entity;
    QQmlComponent component(RenderSystem::m_engine,
    QUrl::fromLocalFile(QString::fromStdString(event.component->resourcePath)));
    QObject *object = component.create();
    QQmlEngine::setObjectOwnership(object, QQmlEngine::CppOwnership);
    QQuickItem *item = qobject_cast<QQuickItem*>(object);
    item->setParent(RenderSystem::m_game_container);
    item->setParentItem(RenderSystem::m_item_container);
    entity.assign<View>(item);
    Game::view_count++;
}
