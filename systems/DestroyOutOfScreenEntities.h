#pragma once
#include "entityx/System.h"

class DestroyOutOfScreenEntities : public entityx::System<DestroyOutOfScreenEntities>
{
public:
	DestroyOutOfScreenEntities();

	void update(entityx::EntityManager &es, entityx::EventManager &events, double dt);
};

