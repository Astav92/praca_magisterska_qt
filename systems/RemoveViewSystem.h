#pragma once
#include "entityx\System.h"

class RemoveViewSystem : public entityx::System<RemoveViewSystem>, public entityx::Receiver<RemoveViewSystem>
{
public:
	RemoveViewSystem();

	void configure(entityx::EventManager &event_manager);

	void update(entityx::EntityManager &entities, entityx::EventManager &events, double dt);

	void receive(const entityx::EntityDestroyedEvent &entity);
};

