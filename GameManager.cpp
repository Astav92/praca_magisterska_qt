#include "GameManager.h"
#include "EntityCreator.h"
#include "components\Enemy.h"
#include "components\Destroy.h"

int GameManager::Player1Lives = 0;
int GameManager::Player2Lives = 0;
int GameManager::Score = 0;
GameState GameManager::State = GameState::MainMenu;

entityx::Entity m_player_one;
entityx::Entity m_player_two;

GameManager *GameManager::Instance = nullptr;

GameManager::GameManager(entityx::EntityManager & entityManager, entityx::EventManager & eventManager)
	:m_entityManager(entityManager),m_eventManager(eventManager)
{
	Instance = this;
	init();
}

void GameManager::init()
{
	m_eventManager.subscribe<EvGameStarted>(*this);
	m_eventManager.subscribe<EvGameEnded>(*this);
	m_eventManager.subscribe<EvPlayerReceivedDamage>(*this);
	m_eventManager.subscribe<EvPlayerKilledEnemy>(*this);
}

void GameManager::receive(const EvGameStarted& ev)
{
	if (ev.numPlayers == 1)
	{
		Player1Lives = 5;

		m_player_one = m_entityManager.create();
		PlayerCreator::create(m_player_one, 1);
	}
	else
	{
		Player1Lives = 5;
		Player2Lives = 5;

		m_player_one = m_entityManager.create();
		PlayerCreator::create(m_player_one, 1);

		m_player_two = m_entityManager.create();
		PlayerCreator::create(m_player_two, 2);
	}

	Score = 0;
	State = GameState::Gameplay;

	m_eventManager.emit<EvUiChanged>();
}

void GameManager::receive(const EvGameEnded& ev)
{
	State = GameState::GameEnd;

	Enemy::Handle enemy;
	Destroy::Handle destroy;

	for (entityx::Entity en : m_entityManager.entities_with_components(enemy))
	{
		destroy = en.component<Destroy>();
		if (!destroy)
		{
			en.assign<Destroy>();
		}
	}
}

void GameManager::receive(const EvPlayerReceivedDamage& ev)
{
  return;
	if (ev.playerId == 1 && Player1Lives > 0)
	{
		Player1Lives--;

		if (Player1Lives <= 0)
		{
			m_player_one.assign<Destroy>();
		}
	}
	else if(Player2Lives > 0)
	{
		Player2Lives--;

		if (Player2Lives <= 0)
		{
			m_player_two.assign<Destroy>();
		}
	}

	if (Player1Lives <= 0 && Player2Lives <= 0)
	{
		m_eventManager.emit<EvGameEnded>();
	}

	m_eventManager.emit<EvUiChanged>();
}

void GameManager::receive(const EvPlayerKilledEnemy & ev)
{
	Score++;
	m_eventManager.emit<EvUiChanged>();
}

void GameManager::StartGame(int numPlayers)
{
  m_eventManager.emit<EvGameStarted>(numPlayers);
}
