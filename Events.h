#pragma once
#include "entityx\Event.h"

struct EvGameStateChanged : public entityx::Event<EvGameStateChanged>
{
	EvGameStateChanged() {}
};

struct EvGameStarted : public entityx::Event<EvGameStarted> {
	int numPlayers;
	EvGameStarted(int players):numPlayers(players){}
	EvGameStarted(){}
};

struct EvPlayerReceivedDamage : public entityx::Event<EvPlayerReceivedDamage>
{
	int damage;
	int playerId;

	EvPlayerReceivedDamage(int dmg, int playerId) :damage(dmg), playerId(playerId){}
};

struct EvGameEnded : public entityx::Event<EvGameEnded>
{
	EvGameEnded(){}
};

struct EvUiChanged : public entityx::Event<EvUiChanged>
{
	EvUiChanged(){}
};

struct EvPlayerKilledEnemy : public entityx::Event<EvPlayerKilledEnemy>
{
	EvPlayerKilledEnemy(){}
};