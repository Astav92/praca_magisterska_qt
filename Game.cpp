#include "Game.h"
#include <chrono>

#include "systems/PlayerControlSystem.h"
#include "systems/AddViewSystem.h"
#include "systems/MoveSystem.h"
#include "systems/RemoveViewSystem.h"
#include "systems/RenderSystem.h"
#include "systems/CollisionSystem.h"
#include "systems/GunSystem.h"
#include "systems/PositionBoundSystem.h"
#include "systems/DestroyOutOfScreenEntities.h"
#include "systems/DestroyEntitySystem.h"
#include "systems/BackgroundSystem.h"
#include "systems/SpawnEnemiesSystem.h"

#include "components\Position.h"
#include "components\Velocity.h"
#include "components\Player.h"
#include "components\Asset.h"
#include "components\BoxCollider.h"
#include "components\Bullet.h"
#include "components\Background.h"
#include "EntityCreator.h"
#include "ScreenSize.h"

int Game::view_count = 0;
static double MS_PER_UPDATE = 0.016;
double game_time;
int frames_count;
int count_tick;
double avg_fps;
double avg_views;

Game::Game(QGuiApplication& app, QQmlApplicationEngine& engine):m_eventManager(),
    m_entityManager(m_eventManager),
    m_systemManager(m_entityManager,m_eventManager),
    m_game_manager(m_entityManager, m_eventManager),
    m_app(app), m_engine(engine)
{
}

void Game::init()
{
	createSystems();

	auto bg1 = m_entityManager.create();
	bg1.assign<Position>(0, 0);
    bg1.assign<Asset>(1024, 1024, ":/bg.qml");
    bg1.assign<Background>(50, -512);

	auto bg2 = m_entityManager.create();
    bg2.assign<Position>(0, -512);
    bg2.assign<Asset>(1024, 1024, ":/bg.qml");
    bg2.assign<Background>(50, -512);
}

void Game::run()
{
	// zegarek
	auto previousTime = std::chrono::high_resolution_clock::now();

	double lag = 0;

	while (true)
	{
		auto currentTime = std::chrono::high_resolution_clock::now();

		auto elapsed = currentTime - previousTime;

		auto elapsed_float = std::chrono::duration_cast<std::chrono::duration<double>>(elapsed);

		previousTime = currentTime;

		lag += elapsed_float.count();

		processInput();

		while (lag >= MS_PER_UPDATE)
		{
			update(MS_PER_UPDATE);
			lag -= MS_PER_UPDATE;
		}

		render(lag);

		game_time += elapsed_float.count();
		frames_count++;

		if (game_time > 1)
		{
		    count_tick++;

		    avg_fps += (frames_count - avg_fps) / count_tick;
		    avg_views += (view_count - avg_views) / count_tick;

		    game_time -= 1;
		    frames_count = 0;


		    qDebug() << avg_fps << "fps, view entities: " << avg_views << "\n";
		}
	}
}

void Game::createSystems()
{
	m_systemManager.add<PlayerControlSystem>();
	m_systemManager.add<MoveSystem>();
	m_systemManager.add<RenderSystem>(m_app, m_engine, m_eventManager);
	m_systemManager.add<AddViewSystem>();
	m_systemManager.add<RemoveViewSystem>();
	m_systemManager.add<CollisionSystem>();
	m_systemManager.add<GunSystem>();
	m_systemManager.add<PositionBoundSystem>();
	m_systemManager.add<DestroyOutOfScreenEntities>();
	m_systemManager.add<DestroyEntitySystem>();
	m_systemManager.add<BackgroundSystem>();
	m_systemManager.add<SpawnEnemiesSystem>();

	m_systemManager.configure();
}

void Game::processInput()
{
}

void Game::update(double ms)
{
	m_systemManager.update<PlayerControlSystem>(ms);
	m_systemManager.update<GunSystem>(ms);
	m_systemManager.update<MoveSystem>(ms);
	m_systemManager.update<BackgroundSystem>(ms);
	m_systemManager.update<DestroyOutOfScreenEntities>(ms);
	m_systemManager.update<PositionBoundSystem>(ms);
	m_systemManager.update<CollisionSystem>(ms);
	m_systemManager.update<DestroyEntitySystem>(ms);
	m_systemManager.update<SpawnEnemiesSystem>(ms);
}

void Game::render(double ms)
{
	m_systemManager.update<RenderSystem>(ms);
}

void Game::exit()
{
}
