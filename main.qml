import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 2.2

Window {
    signal playerOneClicked()
    signal playerTwoClicked()
    signal keyPressed(int code)
    signal keyReleased(int code)

    visible: true
    width: 800
    height: 600
    objectName: "MainWindow"
    id: gameWindow
    title: "Wersja Qt"

    property int frame: 0

    Item {
        id: gameContainer
        z: -1
        anchors.fill: parent
        focus: true

        Keys.onPressed: {
            keyPressed(event.key);
        }
        Keys.onReleased: {
            keyReleased(event.key);
        }

        function setPlayerOneHp(lives)
        {
            playerOneLives.text = "Player 1 lives: " + lives;
        }

        function setPlayerTwoHp(lives)
        {
            playerTwoLives.text = "Player 2 lives: " + lives;
        }

        function setScore(score)
        {
            scoreText.text = "Score: " + score;
        }

        function showHud()
        {
            playerOneLives.visible = true;
            playerTwoLives.visible = true;
            scoreText.visible = true;

            btOnePlayer.visible = false;
            btTwoPlayers.visible = false;
        }

        function hideHud()
        {
            playerOneLives.visible = false;
            playerTwoLives.visible = false;
            scoreText.visible = false;

            btOnePlayer.visible = true;
            btTwoPlayers.visible = true;
        }
    }

    Rectangle {
        id: rectangle
        width: 800
        height: 600
        color: "#00000000"



        Text {
            id: playerOneLives
            x: 13
            y: 513
            width: 183
            height: 77
            color: "#f77676"
            text: qsTr("Player 1 lives: 5")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            font.pixelSize: 20
        }

        Text {
            id: playerTwoLives
            x: 607
            y: 513
            width: 183
            height: 77
            color: "#f77676"
            text: qsTr("Player 2 lives: 5")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignRight
            font.pixelSize: 20
        }

        Text {
            id: scoreText
            x: 299
            y: 8
            width: 183
            height: 77
            color: "#ff8484"
            text: qsTr("Score: 23")
            styleColor: "#fa8a8a"
            font.bold: true
            font.family: "Arial"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 23
        }
    }

//    Text {
//        property real t

//        color: "red"
//        text: "? Hz"

//        Timer {
//            id: fpsTimer
//            property real fps: 0
//            repeat: true
//            interval: 1000
//            running: true
//            onTriggered: {
//                parent.text = "FPS: " + fpsTimer.fps
//                fps = frame
//                frame = 0
//            }
//        }

//        NumberAnimation on t {
//            id: tAnim
//            from: 0
//            to: 100
//            loops: Animation.Infinite
//        }

//        onTChanged: {
//            update() // force continuous animation
//            ++frame
//        }
//    }

    Button {
        id: btOnePlayer
        x: 163
        y: 274
        text: qsTr("One player")
        checkable: false
        rotation: 0
        z: 0
    }


    Button {
        id: btTwoPlayers
        x: 493
        y: 280
        text: qsTr("Two players")
        enabled: true
    }




    Connections {
        target: btOnePlayer
        onClicked: {
            playerOneClicked();
            gameContainer.focus = true;
        }
    }

    Connections {
        target: btTwoPlayers
        onClicked: {
            playerTwoClicked();
            gameContainer.focus = true;
        }

    }



}
