TEMPLATE = app

QT += qml quick
CONFIG += c++11

SOURCES += main.cpp \
    components/Asset.cpp \
    components/Background.cpp \
    components/BoxCollider.cpp \
    components/Bullet.cpp \
    components/Destroy.cpp \
    components/Enemy.cpp \
    components/Gun.cpp \
    components/Health.cpp \
    components/Player.cpp \
    components/Position.cpp \
    components/PositionBound.cpp \
    components/Velocity.cpp \
    components/View.cpp \
    systems/AddViewSystem.cpp \
    systems/BackgroundSystem.cpp \
    systems/CollisionSystem.cpp \
    systems/DestroyEntitySystem.cpp \
    systems/DestroyOutOfScreenEntities.cpp \
    systems/GunSystem.cpp \
    systems/MoveSystem.cpp \
    systems/PlayerControlSystem.cpp \
    systems/PositionBoundSystem.cpp \
    systems/RemoveViewSystem.cpp \
    systems/RenderSystem.cpp \
    systems/SpawnEnemiesSystem.cpp \
    EntityCreator.cpp \
    Game.cpp \
    GameManager.cpp \
    Input.cpp \
    Rect2D.cpp \
    ScreenSize.cpp \
#    entityx/deps/Dependencies_test.cc \
#    entityx/help/Pool.cc \
#    entityx/help/Pool_test.cc \
#    entityx/help/Timer.cc \
#    entityx/tags/TagsComponent_test.cc \
#    entityx/Benchmarks_test.cc \
#    entityx/Entity.cc \
#    entityx/Entity_test.cc \
#    entityx/Event.cc \
#    entityx/Event_test.cc \
#    entityx/System.cc \
#    entityx/System_test.cc
    qteventhandler.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    components/Asset.h \
    components/Background.h \
    components/BoxCollider.h \
    components/Bullet.h \
    components/Destroy.h \
    components/Enemy.h \
    components/Gun.h \
    components/Health.h \
    components/Player.h \
    components/Position.h \
    components/PositionBound.h \
    components/Velocity.h \
    components/View.h \
    systems/AddViewSystem.h \
    systems/BackgroundSystem.h \
    systems/CollisionSystem.h \
    systems/DestroyEntitySystem.h \
    systems/DestroyOutOfScreenEntities.h \
    systems/GunSystem.h \
    systems/MoveSystem.h \
    systems/PlayerControlSystem.h \
    systems/PositionBoundSystem.h \
    systems/RemoveViewSystem.h \
    systems/RenderSystem.h \
    systems/SpawnEnemiesSystem.h \
    EntityCreator.h \
    Events.h \
    Game.h \
    GameManager.h \
    Input.h \
    Rect2D.h \
    resource.h \
    ScreenSize.h \
    entityx/3rdparty/catch.hpp \
    entityx/3rdparty/simplesignal.h \
    entityx/deps/Dependencies.h \
    entityx/help/NonCopyable.h \
    entityx/help/Pool.h \
    entityx/help/Timer.h \
    entityx/tags/TagsComponent.h \
    entityx/config.h \
    entityx/config.h.in \
    entityx/Entity.h \
    entityx/entityx.h \
    entityx/Event.h \
    entityx/quick.h \
    entityx/System.h \
    qteventhandler.h

DISTFILES += \
    Libs/entityx.lib \
    Libs/entityx-d.lib \
    Libs/entityx.dll

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/Libs/ -lentityx
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/Libs/ -lentityx-d

INCLUDEPATH += $$PWD/entityx
DEPENDPATH += $$PWD/entityx


win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/Libs/libentityx.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/Libs/libentityx.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/Libs/entityx.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/Libs/entityx-d.lib
