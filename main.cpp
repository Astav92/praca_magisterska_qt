#define QT_QML_DEBUG
#include "Game.h"
#include <QGuiApplication>
#include <QQmlApplicationEngine>

int main(int argc, char *argv[])
{
    // stworzenie aplikacji qml
    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;
    // późniejsze wykorzystanie aplikacji w systemie
    // renderowania
    Game game(app, engine);
    game.init();
    game.run();
    return 0;
}
