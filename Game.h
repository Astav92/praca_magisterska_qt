#pragma once

#include "entityx/Event.h"
#include "entityx/Entity.h"
#include "entityx/System.h"
#include "GameManager.h"
#include <QGuiApplication>
#include <QQmlApplicationEngine>


class Game
{
public:
    Game(QGuiApplication &app, QQmlApplicationEngine &engine);

	void init();

	void run();

	static int view_count;

private:

	void createSystems();

	void processInput();

	void update(double ms);

	void render(double ms);

	void exit();

	entityx::EventManager m_eventManager;

	entityx::EntityManager m_entityManager;

	entityx::SystemManager m_systemManager;

	GameManager m_game_manager;

    QGuiApplication& m_app;
    QQmlApplicationEngine& m_engine;
};

