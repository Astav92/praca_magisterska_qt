#ifndef QTEVENTHANDLER_H
#define QTEVENTHANDLER_H

#include <GameManager.h>
#include <QObject>
#include <QDebug>

class QtEventHandler : public QObject
{
    Q_OBJECT
public:
    explicit QtEventHandler(QObject *parent = 0);

public slots:
    void playerOneClicked();
    void playerTwoClicked();
    void keyPressed(int key);
    void keyReleased(int key);
};

#endif // QTEVENTHANDLER_H
