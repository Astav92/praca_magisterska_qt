#pragma once
//#include "entityx\Event.h"
#include "entityx\System.h"
#include "Events.h"

enum GameState{MainMenu, Gameplay, GameEnd};

class GameManager : public entityx::Receiver<GameManager>
{
public:
	GameManager(entityx::EntityManager& entityManager,
		entityx::EventManager& eventManager);

	static int Score;

	static int Player1Lives;

	static int Player2Lives;

	static GameState State;

	void init();

	void receive(const EvGameStarted &ev);

	void receive(const EvGameEnded &ev);

	void receive(const EvPlayerReceivedDamage &ev);

	void receive(const EvPlayerKilledEnemy &ev);

	void StartGame(int numPlayers);

	static GameManager *Instance;

private:
	entityx::EntityManager& m_entityManager;

	entityx::EventManager& m_eventManager;
};
