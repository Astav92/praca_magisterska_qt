#include "EntityCreator.h"
#include "components\Asset.h"
#include "components\BoxCollider.h"
#include "components\Bullet.h"
#include "components\Gun.h"
#include "components\Health.h"
#include "components\Player.h"
#include "components\Position.h"
#include "components\Velocity.h"
#include "components\View.h"
#include "components\Enemy.h"
#include "components\PositionBound.h"

#include "ScreenSize.h"


void BulletCreator::create(entityx::Entity entity)
{
	entity.assign<Position>(0, 0);
	entity.assign<Velocity>(0, 0);
	entity.assign<Asset>(10,20,":/bullet.qml");
	entity.assign<BoxCollider>(10, 10, 0, 0);
	entity.assign<Bullet>(5);
}

void EnemyCreator::create(entityx::Entity entity)
{
	entity.assign<Position>(100, 20);
	entity.assign<Velocity>(0, 0);
	entity.assign<Asset>(50,50, ":/enemy.qml");
	entity.assign<BoxCollider>(50, 50, 0, 0);
	entity.assign<Enemy>();
	entity.assign<Health>(10);
	entity.assign<Gun>(1, -1, 0, 25, 50, true);
}

void AsteroidCreator::create(entityx::Entity entity)
{
	entity.assign<Position>(100, 20);
	entity.assign<Velocity>(0, 0);
	entity.assign<Asset>(80, 80, ":/asteroid.qml");
	entity.assign<BoxCollider>(80, 80, 0, 0);
	entity.assign<Enemy>();
	entity.assign<Health>(10);
}

void PlayerCreator::create(entityx::Entity entity, int id)
{
	entity.assign<Velocity>(0, 0);
	if (id == 1)
	{
	entity.assign<Asset>(50, 50, ":/player.qml");
		entity.assign<Position>(100, 200);
	}
	else
	{
	entity.assign<Asset>(50, 50, ":/player2.qml");
		entity.assign<Position>(300, 200);
	}
	entity.assign<BoxCollider>(50, 50, 0, 0);
	entity.assign<Player>(id);
	entity.assign<Health>(10);
	entity.assign<Gun>(0.3, -1, 0,25,0, false);
	entity.assign<PositionBound>(50, ScreenSize::height-200, ScreenSize::width-100, ScreenSize::height-100);
}
